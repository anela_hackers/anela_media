<!-- 404 -->

<?php get_header(); ?>

<div id="container" class="clear">
	<div id="main" class="float_left">

		
			<article> 
				<hgroup>             
            		<h2>記事が見つかりませんでした。</h2>
           		 	<p>検索で見つかるかもしれません。</p><br />
            		<?php get_search_form(); ?>
				</hgroup>  
			</article> 
		                        
	</div><!-- #main -->
	
<?php get_sidebar(); ?>

</div><!-- #container -->

<?php get_footer(); ?>

</body>
</html>