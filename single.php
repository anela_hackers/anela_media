<?php get_header(); ?>


<div id="container" class="clear">
	
<!-- ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■　個別記事　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■　-->
	
	<div id="main" class="float_left">

		<nav class="bread">
			<?php my_bread(); ?>
		</nav>
 <hr />
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			
			<article>
            
<ul class="cat clear">
					<li class="cat"><?php the_category('') ?></li>
                    </ul>
				<hgroup class="title">
                <p><?php the_time('Y.m.d') ?></p>
					<h1><?php the_title(); ?></h1>
                    
				</hgroup>
       
				
                    <ul class="tag">
					<h4>
											<?php the_tags('<ul class="clear"><li class="float_left">','</li><li class="float_left">','</li></ul>');?>
                                      </h4>
                                      </ul>
                                      <ul>
					
                    </ul>
                    
				

		<div class="post_single">   
			      <p class="thmb"><?php the_post_thumbnail('thumbnails'); ?></p>
			<?php the_content(); ?>

<!-- ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■　ソーシャル　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■　-->
 
<ul class="clear social">
<li class="float_left">
 <a href="https://twitter.com/share" class="twitter-share-button" data-via="scupo_com" data-lang="ja">ツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
 </li>
        <li class="float_left">
<g:plusone size="medium"></g:plusone>

<script type="text/javascript">
  window.___gcfg = {lang: 'ja'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
 </li>
 <li class="float_left">
        <a href="http://b.hatena.ne.jp/entry/http://scupo.com/" class="hatena-bookmark-button" data-hatena-bookmark-title="Scupo" data-hatena-bookmark-layout="standard" title="このエントリーをはてなブックマークに追加"><img src="http://b.st-hatena.com/images/entry-button/button-only.gif" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
        </li>
 <li class="float_left">
 <div class="fb-like" data-href="http://scupo.com/<?php the_permalink() ?>" data-send="true" data-width="450" data-show-faces="true"></div>
  </li>
        </ul>

		<?php endwhile; ?>
		<?php else : ?>
      
			<hgroup>             
				<h2>記事が見つかりませんでした。</h2>
				<p>検索で見つかるかもしれません。</p><br />
					<?php get_search_form(); ?>
			</hgroup>    
 
		<?php endif; ?>
        <nav class="clear prevnext">
		<span class="float_left"><?php previous_post_link('%link','◀︎'); ?></span>
		<span class="float_right"><?php next_post_link('%link','▶︎'); ?></span>
		</nav>
		</div><!-- /.post_single -->            
       
		</article> 
        
	

	
    
</div><!-- #main -->

<?php get_sidebar(); ?>

</div><!-- #container -->

<?php get_footer(); ?>

</body>

</html>

