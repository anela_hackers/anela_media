<!-- category -->

<?php get_header(); ?>
<div id="future">		

<ul>      
<!-- タグ:futureを３件取得 -->  
  <?php query_posts('tag=future&showposts=3');?>
  <?php if(have_posts()):while(have_posts()):the_post();?>
<li>
<hgroup>
                                        <h3>
                                        <!-- カテゴリ名を出力：ここから -->                            
                                        <?php 
                                            $post_cat=get_the_category(); 
                                            $cat_id=$post_cat[0]->cat_name;
                                        ?>
                                        <?php 
                                            $post_cat=get_the_category(); 
                                            $cat=$post_cat[0];
                                        ?>
                                        
                                        <?php echo $cat->cat_name ?>
                                        <!-- カテゴリ名を出力：ここまで --> 
                                        </h3>
                                  </hgroup>       

								<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
									<?php if(has_post_thumbnail()): { echo the_post_thumbnail(thumnails); } ?>
									<?php else:?>
										<img src="<?php bloginfo('template_url'); ?>/images/no_thumb.png" alt="no image" title="no image" width="600" height="315"/>
									<?php endif; ?>
									
								</a>
									
								<hgroup class="title">
									<a href = "<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
										<h2><?php the_title(); ?></h2>
									</a>
                                     
								</hgroup>
</li>  

 <?php endwhile;endif;?>
</ul>

</div><!-- /#future -->
<div id="tit_cat">

	<!-- hogehoge -->  
	<?php if(in_category('hogehoge')) : ?>
		<span class="icon_cat"> <img src="<?php bloginfo('template_url'); ?>/images/hogehoge.png" width="" height="" alt="" /></span>
  
		<!-- hagehage -->  
	<?php elseif(in_category('hagehage')) :?>
		<span class="icon_cat"> <img src="<?php bloginfo('template_url'); ?>/images/hagehage.png" width="" height="" alt="" /></span>

	<?php else :?>
	<?php endif; ?>

</div><!-- /#tit_cat -->  

	<div id="container" class="clear">
    
		<article id="main" class="float_left">
			<h1></h1>
			<ul class="clear">      
	
				<?php if (have_posts())  : query_posts($query_string .'&tag__not_in=-11'); ?> 
                		<!-- タグ:futureをループに表示しない -->  
					<!-- 投稿一覧を出力する前に行う -->
					<?php while (have_posts()) : the_post(); ?>
						<!-- 個々の投稿を出力する処理：ここから　-->
            
						<li class="post float_left">
                            <hgroup>
                                        <h3>
                                        <!-- カテゴリ名を出力：ここから -->                            
                                        <?php 
                                            $post_cat=get_the_category(); 
                                            $cat_id=$post_cat[0]->cat_name;
                                        ?>
                                        <?php 
                                            $post_cat=get_the_category(); 
                                            $cat=$post_cat[0];
                                        ?>
                                        
                                        <?php echo $cat->cat_name ?>
                                        <!-- カテゴリ名を出力：ここまで --> 
                                        </h3>
                                  </hgroup>       

								<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
									<?php if(has_post_thumbnail()): { echo the_post_thumbnail(); } ?>
									<?php else:?>
										<img src="<?php bloginfo('template_url'); ?>/images/no_thumb.png" alt="no image" title="no image" width="280" height="146" />
									<?php endif; ?>
									
								</a>
									
								<hgroup>
									<a href = "<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
										<h2><?php the_title(); ?></h2>
									</a>
                                      <h5><?php echo get_the_date('Y.n.j'); ?></h5>
                                     <h4>
											<?php the_tags('<ul class="clear"><li class="float_left">','</li><li class="float_left">','</li></ul>');?>
                                      </h4>
								</hgroup>
						</li><!-- /.post float_left -->

					<?php endwhile; ?>
                    </ul>
					<!-- 個々の投稿を出力する処理：ここまで　-->
					<?php else : ?><!-- 投稿が無い場合　-->
				
                
			<section id="main_under">
					<hgroup>             
						<h2>記事が見つかりませんでした。</h2>
						<p>検索で見つかるかもしれません。</p>
						<br />
						<?php get_search_form(); ?>
					</hgroup>    
				</section>  
  
		<?php endif; ?>
        
                     <div class="pagination">
                <?php global $wp_rewrite;
                $paginate_base = get_pagenum_link(1);
                if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
                    $paginate_format = '';
                    $paginate_base = add_query_arg('paged','%#%');
                }
                else{
                    $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
                    user_trailingslashit('page/%#%/','paged');;
                    $paginate_base .= '%_%';
                }
                echo paginate_links(array(
                    'base' => $paginate_base,
                    'format' => $paginate_format,
                    'total' => $wp_query->max_num_pages,
                    'mid_size' => 4,
                    'current' => ($paged ? $paged : 1),
                    'prev_text' => '◀︎',
                    'next_text' => '▶︎',
                )); ?>
            </div>
			</article><!-- #main -->
    
			<?php get_sidebar(); ?>

		</div><!-- #container -->


<?php get_footer(); ?>

</body>

</html>
