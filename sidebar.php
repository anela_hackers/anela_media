<!-- side navi -->

<aside id="sidebar" class="float_right"> 
<section>
<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/bnr_shop.png" alt="no image" title="no image"/></a>
</section>
<hr />
	   <section id="categories">
       <h3>CATEGORIES</h3>
       <ul>
	<!-- カテゴリを一覧表示：ここから　-->
    <?php
    $cat_all = get_terms( "category", "fields=all&get=all" );
    foreach($cat_all as $value):
 ?>
<li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name;?></a></li>
<?php endforeach; ?>
</ul>
</section>
<!-- カテゴリを一覧表示：ここまで　-->
<hr />
<section id="tag">
<h3>TAG</h3>
<ul class="clear">
<!-- タグを一覧表示：ここから　-->
<?php
$args = array(
  'order' => 'desc',
  'number' => 20
);
$tags = get_terms('post_tag', $args);
foreach($tags as $value) {
  echo '<li class="float_left"><a href="'. get_tag_link($value->term_id) .'">'. $value->name .' </a></li>';
}
?>
<!-- タグを一覧表示：ここまで　-->
</ul>
</section>
</aside>