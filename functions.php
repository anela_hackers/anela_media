<?php

// ウィジェットエリア
// サイドバーのウィジェット thx for webcreatorbox
register_sidebar( array(
     'name' => __( 'Side Widget' ),
     'id' => 'side-widget',
     'before_widget' => '<li class="widget-container">',
     'after_widget' => '</li>',
     'before_title' => '<h1>',
     'after_title' => '</h1>',
) );

// フッターエリアのウィジェット
register_sidebar( array(
     'name' => __( 'Footer Widget' ),
     'id' => 'footer-widget',
     'before_widget' => '<section><ul><li class="widget-container">',
     'after_widget' => '</li></ul></section>',
     'before_title' => '<h1>',
     'after_title' => '</h1>',
) );

// アイキャッチ画像
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 280, 146, true ); // 通常の投稿サムネイル
add_image_size( 'thumnails', 600, 315, true );

// カスタムナビゲーションメニュー
add_theme_support('menus');

/* more-linkのハッシュ消し*/
function remove_more_jump_link($link) {
  $offset = strpos($link, '#more-');
  if ($offset) {
    $end = strpos($link, '"',$offset);
  }
  if ($end) {
    $link = substr_replace($link, '', $offset, $end-$offset);
  }
  return $link;
}
add_filter('the_content_more_link', 'remove_more_jump_link');


/*ぱんくずメニュー*/
function my_bread(){
     global $post;

     define("SEPA"," > ");     //区切り文字

     //【表示】ホーム
	 echo '<ul class="pankuzu clear">';
	 echo '<li class="float_left">';
     echo '<a href="'. get_bloginfo('url').'">HOME</a>';
	
     //カテゴリアーカイブ
     if(is_category()){
     	echo SEPA;
        $cat = get_category(get_cat_ID(single_cat_title('',false)));
        //親カテゴリがある場合
        if($cat->parent){
        	$cate_pare = get_category_parents(get_cat_ID(single_cat_title('',false)),true,SEPA);
            //get_category_parentsが自カテゴリも生成しちゃうので除去
            for($i=0;$i<2;$i++){
            	$cate_pare = substr($cate_pare,0,strrpos($cate_pare,SEPA));
            }
            //【表示】親カテゴリ
			 echo '<li class="float_left">';
            echo $cate_pare.SEPA;
			 echo '</li>';
        }
        //【表示】自カテゴリ
		 echo '<li class="float_left">';
        single_cat_title();
		 echo '</li>';
     //個別記事ページ
     }elseif(is_single()){
        echo SEPA;
        while(have_posts()){
			
			//【表示】カテゴリ
        	the_post();
            $cat=get_the_category();
            $cat = $cat[0];
			echo '<li class="float_left">';
            echo get_category_parents($cat->cat_ID,true,SEPA);
			echo '</li>';
		
            //【表示】記事タイトル
			echo '<li class="float_left">';
            the_title();
			echo '</li>';
				
			echo '</ul>';
        }
     //ページ
     }elseif(is_page()){
        //【表示】ページタイトル
		echo '<li class="float_left">';
        the_title_attribute();
		echo '</li>';
		echo '</ul>';
     //タグアーカイブ   
     }elseif(is_tag()){
     	echo SEPA.'タグ：';
        //タグ名
		echo '<li class="float_left">';
        single_tag_title();
		echo '</li>';
		echo '</ul>';   
     //日付アーカイブ
     }elseif(is_date()){
     	echo SEPA;
        //西暦の後に「年」がつかないので追加   
        $date = wp_title('',false);
        $date = substr($date,0,6).'年'.substr($date,6);
        //【表示】日付
		echo '<li class="float_left">';
        echo $date;
        //日付の後に「日」がつかないので追加
        if(is_day()){
        	echo '日';
			echo '</li>';
			echo '</ul>'; 
        }
     //検索結果
     }elseif(is_search()){
     	echo SEPA;
        //【表示】検索文字列
		echo '<li class="float_left">';
        the_search_query();
		echo '</li>';
		echo '<li class="float_left">';
        echo '&nbsp;の検索結果';
		echo '</li>';
		echo '</ul>'; 
     //404ページ
     }elseif(is_404()){
        //【表示】案内文
		echo '<li class="float_left">';
        echo SEPA.'ページが見つかりません。';	
		echo '</li>';
		echo '</ul>';
     }
	 
	 
}

//RSS feedの抜粋配信に画像を追加
function rss_post_thumbnail($content) {
  global $post;
  if(has_post_thumbnail($post -> ID)) {
    $content = '<p>' . get_the_post_thumbnail($post -> ID) .
    '</p>' . get_the_excerpt();
  }
  return $content;
}
add_filter('the_excerpt_rss', 'rss_post_thumbnail');

//ファビコンwp_headにフック
function blog_favicon() {
  echo '<link rel="shortcut icon" type="image/x-icon" href="'.get_bloginfo('template_url').'/images/favicon.ico" />'."\n";
}
add_action('wp_head', 'blog_favicon');

//管理画面にもファビコン
function admin_favicon() {
  echo '<link rel="shortcut icon" type="image/x-icon" href="'.get_bloginfo('template_url').'/images/admin-favicon.icon" />';
}
add_action('admin_head', 'admin_favicon');

//投稿画像サイズ制限
function filter_editor_max_image_size() {
  return array(600, 9999);
}
add_filter('editor_max_image_size', 'filter_editor_max_image_size');

//カテゴリー制限(only one category)
function my_print_footer_scripts() {
echo '<script type="text/javascript">
  //<![CDATA[
  jQuery(document).ready(function($){
    $(".categorychecklist input[type=checkbox]").each(function(){
      $check = $(this);
      var checked = $check.attr("checked") ? \' checked="checked"\' : \'\';
      $(\'<input type="radio" id="\' + $check.attr("id")
        + \'" name="\' + $check.attr("name") + \'"\'
  	+ checked
	+ \' value="\' + $check.val()
	+ \'"/>\'
      ).insertBefore($check);
      $check.remove();
    });
  });
  //]]>
  </script>';
}
add_action('admin_print_footer_scripts', 'my_print_footer_scripts', 21);

//ナビメニュー
add_theme_support('menus');

/* タイトル無いときに表示 */
function set_the_title($title = "") {
    $sNoTitle = "(no title)";

    if(strlen($title) === 0) {
        return $sNoTitle;
    }
    return $title;
}
add_filter('the_title', 'set_the_title');

?>
