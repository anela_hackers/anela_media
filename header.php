<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja" dir="ltr">

<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title> 

<!-- metadata -->
<meta name="copyright" content="metamon. All Rights Reserved." />
<meta name="keywords" content="metamon" />
<meta name="description" content="metamon" />

<!-- Favicon -->
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">

<!-- stylesheet -->
<meta http-equiv="content-style-type" content="text/css" />
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/import.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/index.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/jquery.lightbox-0.5.css" media="screen" />

<!--[if IE 6]>
<script type="text/javascript" charset="utf-8">
DD_belatedPNG.fix("img");
</script>

<![endif]-->
<!-- IE6,7,8にhtml5の要素を適応-->
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>js/html5.js" ></script>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<style type="text/css">
article,aside,canvas,details,figcaption,figure,
footer,header,hgroup,menu,nav,section,summary
{
	display:block;
}
</style>

<?php wp_head(); ?>

<?php wp_deregister_script('jquery'); ?>
<!-- WordPressのjQueryを読み込ませない -->
 
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<!-- ロールオンでフェード -->
<script> 
jQuery(function() {
var nav = jQuery('#future li');
nav.hover(
	function(){
		nav.not(this).stop().fadeTo(200,0.6);
	},function () {
		nav.not(this).stop().fadeTo(200,1);
	});
});
</script> 
<script> 
jQuery(function() {
var nav = jQuery('li.post');
nav.hover(
	function(){
		nav.not(this).stop().fadeTo(200,0.6);
	},function () {
		nav.not(this).stop().fadeTo(200,1);
	});
});
</script> 

</head>

	<body <?php body_class(); ?>>
		<div id="wrapper">
		
			<header>				
				<hgroup>
                <p>←anela shop</p>
				<h1 id="logo">
					<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/anelamagazine_logo.png" alt="no image" title="no image"/></a>
				</h1>
			</hgroup>      

		</header>